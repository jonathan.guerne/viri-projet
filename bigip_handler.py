from f5.bigip import ManagementRoot

class Bigip_handler():

    def __init__(self):
        self.mgmt = None


    def connect(self, srv, name, psw, port):
        # Connect to the BigIP
        try :
            self.mgmt = ManagementRoot(srv, name, psw, port=port)
            print('successfully connected to the bigip server')
        except :
            print('Error while trying to connect to the bigip server')

    def get_virtuals(self):
        assert self.mgmt is not None
        return self.mgmt.tm.ltm.virtuals.get_collection()

    def create_virutals(self, **kwargs):
        assert self.mgmt is not None
        self.mgmt.tm.ltm.virtuals.virtual.create(**kwargs)
    
    def delete_virtuals(self, **kwargs):
        assert self.mgmt is not None
         # Delete a pool if it exists
        if self.mgmt.tm.ltm.virtuals.virtual.exists(**kwargs):
            _v = self.mgmt.tm.ltm.virtuals.virtual.load(**kwargs)
            _v.delete()

    def get_nodes(self):
        assert self.mgmt is not None
        return self.mgmt.tm.ltm.nodes.get_collection()

    def create_nodes(self, **kwargs):
        assert self.mgmt is not None
        self.mgmt.tm.ltm.nodes.node.create(**kwargs)
    
    def delete_nodes(self, **kwargs):
        assert self.mgmt is not None
         # Delete a pool if it exists
        if self.mgmt.tm.ltm.nodes.node.exists(**kwargs):
            _v = self.mgmt.tm.ltm.nodes.node.load(**kwargs)
            _v.delete()
    
    def get_pools(self):
        assert self.mgmt is not None
        return self.mgmt.tm.ltm.pools.get_collection()

    def create_pools(self, **kwargs):
        assert self.mgmt is not None
        self.mgmt.tm.ltm.pools.pool.create(**kwargs)
    
    def delete_pools(self, **kwargs):
        assert self.mgmt is not None
         # Delete a pool if it exists
        if self.mgmt.tm.ltm.pools.pool.exists(**kwargs):
            _v = self.mgmt.tm.ltm.pools.pool.load(**kwargs)
            _v.delete()


    