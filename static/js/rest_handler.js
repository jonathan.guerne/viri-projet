function handle_post_request(element, address, relocate){
    
    dataToSend = getFormData(element);
    console.log(dataToSend)

    $.ajax({
        url: address,
        dataType: 'text',
        type: 'POST',
        contentType: 'application/json',
        data:dataToSend,
        crossDomain: true,
        processData: false,
        success: function( ){
            console.log('success')
            window.location.replace(relocate);
        },
        error: function( jqXhr, textStatus, errorThrown ){
            console.log( errorThrown );
        }
    });
    return false
}


// extract data from form to get them into dict format 
function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return JSON.stringify(indexed_array);
}