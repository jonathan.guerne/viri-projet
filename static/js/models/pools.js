$.ajax({ 
    type: "GET",
    url: "http://localhost:5000/api/pools",
    datatype: "json",
    success: function(data){ 
        console.log(data)
        table = document.getElementById("pool_table")
        data.forEach(el => {
            // Create an empty <tr> element and add it to the 1st position of the table:
            var row = table.insertRow(-1);

            // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            

            // Add some text to the new cells:
            cell1.innerHTML = el.name;
            cell2.innerHTML = el.partition;
            cell3.innerHTML = '<button onclick="vips_delete(\''+el.name+'\',\''+el.partition+'\')" class="btn btn-warning">Delete Entry</button>';

        });

    }
 });


 function vips_delete(name, partition){
    $.ajax({
        url: '/api/pools/'+partition+'/'+name,
        type: 'DELETE',
        contentType: 'application/json',
        crossDomain: true,
        success: function( ){
            console.log('success')
            location.reload();
        },
        error: function( jqXhr, textStatus, errorThrown ){
            console.log( errorThrown );
        }
    });

 }