from flask import Flask, render_template, url_for, jsonify
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS
from bigip_handler import Bigip_handler

app = Flask(__name__)
api = Api(app)
CORS(app, origins="http://127.0.0.1:5000", allow_headers=[
    "Content-Type", "Authorization", "Access-Control-Allow-Credentials"],
    supports_credentials=True)

bigip_handler = Bigip_handler()


vip_parser = reqparse.RequestParser()
vip_parser.add_argument('name')
vip_parser.add_argument('partition')
vip_parser.add_argument('source')
vip_parser.add_argument('destination')
vip_parser.add_argument('pool')

node_parser = reqparse.RequestParser()
node_parser.add_argument('name')
node_parser.add_argument('partition')
node_parser.add_argument('address')

pool_parser = reqparse.RequestParser()
pool_parser.add_argument('name')
pool_parser.add_argument('partition')


# def abort_if_vip_doesnt_exist(vip):
#     abort(404, message="VIP {} doesn't exist".format(todo_id))

## API to Handler => ah 

class ah_VIP(Resource):

    def get(self, vip_partition, vip_name):
        return [vip_partition, vip_name]

    def delete(self, vip_partition, vip_name):
        try :
            req = {'partition': vip_partition, 'name': vip_name}
            print(req)
            bigip_handler.delete_virtuals(**req)
            return 'Virtual deleted'
        except Exception as e:
             print(e)
             return 'Error on virtual suppresion'
 

class ah_VIP_list(Resource):
    
    def get(self):
        collect = bigip_handler.get_virtuals()
        raw_data = []
        
        for entry in collect :
            raw_entry = entry.raw
            raw_data.append({
                'name' : raw_entry['name'],
                'partition' : raw_entry['partition']})


        return jsonify(raw_data)

    def post(self):
        try :
            args = vip_parser.parse_args()
            bigip_handler.create_virutals(**args)
            return 'Virtual created'
        except Exception as e:
            print('error on virtual creation')
            print(e)
            return 'Error on virtual creation'


class ah_NODE(Resource):

    def get(self, node_partition, node_name):
        return [node_partition, node_name]

    def delete(self, nodes_partition, nodes_name):
        try :
            req = {'partition': nodes_partition, 'name': nodes_name}
            print(req)
            bigip_handler.delete_nodes(**req)
            return 'Node deleted'
        except Exception as e:
             print(e)
             return 'Error on node suppresion'

class ah_NODE_list(Resource):
    
    def get(self):
        collect = bigip_handler.get_nodes()
        raw_data = []
        
        for entry in collect :
            raw_entry = entry.raw
            raw_data.append({
                'name' : raw_entry['name'],
                'partition' : raw_entry['partition']})


        return jsonify(raw_data)

    def post(self):
        try :
            args = node_parser.parse_args()
            bigip_handler.create_nodes(**args)
            return 'Node created'
        except Exception as e:
            print('error on node creation')
            print(e)
            return 'Error on node creation'


class ah_POOL(Resource):

    def get(self, pool_partition, pool_name):
        return [pool_partition, pool_name]

    def delete(self, pool_partition, pool_name):
        try :
            req = {'partition': pool_partition, 'name': pool_name}
            bigip_handler.delete_pools(**req)
            return 'Pool deleted'
        except Exception as e:
             print(e)
             return 'Error on pool suppresion'

class ah_POOL_list(Resource):
    
    def get(self):
        collect = bigip_handler.get_pools()
        raw_data = []
        
        for entry in collect :
            raw_entry = entry.raw
            raw_data.append({
                'name' : raw_entry['name'],
                'partition' : raw_entry['partition']})

        return jsonify(raw_data)

    def post(self):
        try :
            args = pool_parser.parse_args()
            print(args)
            bigip_handler.create_pools(**args)
            return 'Pool created'
        except Exception as e:
            print('error on pool creation')
            print(e)
            return 'Error on pool creation'

##
## Actually setup the Api resource routing here
##
api.add_resource(ah_VIP_list, '/api/vips')
api.add_resource(ah_VIP, '/api/vips/<vip_partition>/<vip_name>')

api.add_resource(ah_NODE_list, '/api/nodes')
api.add_resource(ah_NODE, '/api/nodes/<nodes_partition>/<nodes_name>')

api.add_resource(ah_POOL_list, '/api/pools')
api.add_resource(ah_POOL, '/api/pools/<pool_partition>/<pool_name>')


@app.route('/nodes')
def get_nodes():
    return render_template('/nodes/get.html')

@app.route('/nodes/new')
def post_nodes():
    return render_template('nodes/post.html')

@app.route('/pools')
def get_pools():
    return render_template('/pools/get.html')

@app.route('/pools/new')
def post_pools():
    return render_template('pools/post.html')

@app.route('/vips')
def get_vips():
    return render_template('/vips/get.html')

@app.route('/vips/new')
def post_vips():
    return render_template('/vips/post.html')

@app.route('/')
def home():
    return render_template('index.html')

if __name__ == '__main__' :

    bigip_handler.connect('localhost','vagrant','vagrant','10443')
    app.run(debug=True)