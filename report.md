---
title: "Projet : Automate F5 Application Delivery Controller configuration via REST"
author: [Jonathan Guerne]
date: "2019-04-04"
subtitle: "MSE - VIRI 2019"
lang: "fr"
titlepage: true
titlepage-rule-color: E1001A
titlepage-rule-height: 2
toc-own-page: true
toc: true
logo: C:/Users/Jonathan/Pictures/mse.png
logo-width: 200
listings: true
listings-no-page-break: true
numbersections: true
---

# Introduction 

L'objectif de ce projet est de développé un interface REST permettant d'interagir avec des appareils F5. 
Le script devrait pouvoir permettre les interactions suivantes : 

1. ajouter / supprimer des VIP
2. ajouter / supprimer / gérer les membres du serveurs 
3. ajouter / supprimer des moniteurs
4. activer / désactiver le reverse proxy
5. activer / désactiver WAF
6. importer / exporter les configurations (CSV, JSON, YAML)

Pour démontrer le bon fonctionnement du système il sera demandé de mettre en place un site web simple permettant d'interagir avec l'api. 

On va utiliser Vagrant pour simplifier la mise en place du serveur F5. 

## Vagrant

Selon Wikipedia : "Vagrant est un logiciel libre et open-source pour la création et la configuration des environnements de développement virtuel. Il peut être considéré comme un wrapper autour de logiciels de virtualisation comme VirtualBox."

Vagrant est un outil très utilisé pour accélérer le développement logiciel. Un peu à la manière de docker, il permet d'instancier et d'utiliser des machines virtuelles déjà configurés avec du logiciel ou des framework spécifique. La grande différence entre Docker et Vagrant c'est que Docker instancie des containers et Vagrant utilise lui directement des machines virtuelles.

Ces VMs accessibles (utilisables) par vagrant sont disponible sur un dépôt en ligne. On parle de box pour définir une VMs Vagrant. 

### Containers vs VMs

![Containers vs VMs](images/container_vs_vms.png)

Comme le démontre l'illustration ci-dessus les controller communique directement avec l'OS hôte sans passer par un hyperviseur. Plus intéressant encore on remarque qu'on container ne contient pas lui-même de d'OS (à l'instar des machines virtuelles). Tout ceci conduit au fait que les containers sont globalement bien plus léger et rapide à instancier que les machines virtuelles. 

Cependant le fait que les machines virtuelles soit des systèmes complet nous permets d'interagir complètement avec le système qu'elles embarques. On gagne aussi en sécurité en utilisant des machines virtuelles car on a la possibilité d'utiliser des systèmes déjà bien établis. 

# Boxe F5-BIGIP 

Comme spécifié dans l'introduction pour simplifier la mise en place du notre projet on va utiliser une box contenant directement une installation d'un serveur F5.

Voici là box qui à été retenu :

https://app.vagrantup.com/boeboe/boxes/F5-BIGIP

Si maintenant on tente d'exécuter la machine virtuelle sans plus ample configuration on va très vite remarquer que ça ne fonctionne pas. L'image est téléchargée correctement mais impossible d'atteindre la page d'administration du serveur, de plus on nous signale quelques erreur (mineurs) à l'instanciation. 

Après quelques recherche on comprend que les erreurs sont liés à un manque de configuration de l'image à l'instanciation. Pour réussir à faire fonctionner ce serveur le tutoriel disponible sur le dépôt suivant à été très utile :

https://github.com/f5devcentral/f5-vagrant-files/tree/master/bigip-13.0.0

Suivre ce modèle de configuration à permis d'atteindre la page de configuration du serveur. Mais il reste encore une dernière étape. Pour pouvoir utiliser un produit F5 comme ici il vaut posséder une licence. Pour résoudre ce problème il a suffit de se créer un compte et d'appliquer pour une licence "trial". 

# F5 SDK
F5 sdk est une libraire python qui simplifie l'interaction avec des réseaux BIG-IP F5. 

On installe la libraire avec la commande suivante : 

``` shell
pip install f5-sdk
```

Cette libraire va être utilisée dans le backend de notre application pour permettre la communication entre notre future api et le serveur BIG-IP.


# API REST

Le but du projet est de réussir à mettre en place une api REST permettant d'interagir avec les fonction décrite dans le chapitre précédent. Pour simplifier la mise en place de l'api on utilise flask_restful. C'est une libraire, qui complémente flask, qui sert à créer des apis REST. Elle simplifie le travail du développeur car elle lui permet de mettre en place son api en implémentant uniquement des classes (qui héritent s'une superclasse `Ressource`) et qui définissent la logique d'ajout, de suppression ou encore de modification des objets en question. La mise en place des routes est transparente pour le développeur. 

Dans notre cas on va développer une classe par élément avec lesquels on souhaite interagir (VIP, membre du réseau, moniteur, ...). Cette classe servira de lien entre le réseau BIG-IP et l'api. Pour chaque classe on va, au besoin, implémenter les méthodes d'ajout, de suppression et de modification. Ces méthodes seront implémentées en utilisant la librairie f5-sdk présentée au chapitre précédent.  


Tous les urls liés à l'apis REST seront précédés par le mot-clés `apis`

# Frontend

Pour pouvoir interagir avec la librairie f5-sdk et donc le réseau BIG-IP en lui-même on va créer une interface graphique. Cette interface graphique sera basée sur Flask, un framework python qui simplifie la mise en place de petit site web. 

